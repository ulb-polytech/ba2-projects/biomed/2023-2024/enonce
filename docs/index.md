# Caméra Thermique Infrarouge

Les caméras thermiques permettent de mesurer les températures des objets présents dans leur champ de vue grâce au rayonnement infrarouge émis par ceux-ci. Ce principe est également utilisé pour les thermomètres sans contacts qui permettent de mesurer la température à distance. Lors de la pandémie de COVID-19, ce type de dispositifs a été fortement utilisé pour identifier les personnes fièvreuses et donc potentiellement infectées.

L’objectif de ce projet est de réaliser un dispositif permettant de restituer une image thermique à l’aide d’un capteur monopoint relativement bon marché. Cette caméra permettra, par exemple, de réaliser l’image thermique d’une personne immobile mais également d’objets divers, voir de bâtiments pour évaluer leur efficacité énergétique. D’un point de vue médical, mesurer la température des différentes parties d’un corps a plusieurs intérêts, par exemple, identifier de la fièvre, des troubles vasculaires périphériques, etc. Le dispositif ainsi créé pourra être relié directement à un ordinateur afin de générer, traiter et afficher l’image thermique finale. Idéalement, il devrait pouvoir être aussi utilisable de manière autonome.

Nous présentons ci-dessous deux exemples d’images thermiques. Notez que les gammes de températures en fonction des couleurs ne sont pas identiques. 

![Exemple d'images thermiques](img/images_thermiques.png)


##  Cahier des charges 

Le dispositif à réaliser sera principalement composé d'un microcontrôleur, de deux moteurs permettant le balayage (pan/tilt) et d'un capteur infrarouge. Il sera relié à un ordinateur et/ou pourra fonctionner en wifi avec un smartphone (voir avec un système de contrôle local constitué, par exemple, de boutons, d'un écran tactile, etc.). Nous reprenons ci-dessous le schéma de principe du dispositif à concevoir.

![Schéma de principe](img/schema_principe.png)

Le cahier des charges reprenant les différents objectifs du projet est le suivant :

- Réaliser une recherche bibliographique des principes physiques et électriques impliqués dans la mesure de température avec un thermomètre sans contacts.
- Faire un petit état de l'art de l'utilisation de la thermographie infrarouge dans le domaine biomédical.
- Le dispositif final construit devra répondre aux caractériques techniques suivantes :
    - **Précision** de la mesure de température **inférieure à 1-2°C** pour les applications médicales.
	- Plage de mesures de températures de -10° à 110°C.
	- Champ de vue complet de la caméra de minimum 80° sur les deux axes.
	- Résolution **angulaire** du capteur de température infrarouge de **maximum 5°**.
	- Résolution de l'image thermique de mininum 80x80 (pixels).
	- Temps d'acquisition complet d'une image de maximum 6 minutes.
	- Utilisation d'un microcontrôleur compatible avec l'[ARDUINO IDE](https://www.arduino.cc/).
	- Création d’un programme pouvant piloter le dispositif (démarrage, configuration, ...), traiter les acquisitions, les enregistrer et les afficher sur un ordinateur, de préférence en python (et)ou intégré directement au microcontrôleur qui devra être alors choisi judicieusement pour pouvoir effectuer cette tâche. 
	- Coût total du dispositif inférieur à 100€.
- Pour chaque composant du dispositif (c.à.d. moteurs, capteur de température sans contacts et microcontrôleur), identifier et comparer au moins 3 composants à l'aide de tableaux reprenant des critères découlant du cahier des charges et qui devront être expliqués/justifiés (exemple : le prix). Le choix final de chaque composant se fera alors sur base de ces tableaux comparatifs.
- La précision des mesures du capteur de température devra être évalué pratiquement en prenant en compte les points suivants :
    - Une expérience devra être imaginée afin de pouvoir évaluer, le plus automatiquement possible, la précision du capteur pour des valeurs corporelles les plus extrêmes d'un patient, voir sur une plage de températures encore plus large.
	- Le matériel, les conditions et les paramètres de l'expérience devront être préalablement bien étudiés, décrits et adaptés aux principes physiques rentrant en jeu.
	- Si la précision pratique du capteur de température est inférieure à celle donnée dans sa _datasheet_, les causes de cette imprécision devront être identifiées et analysées et des pistes d'amélioration de la précision devront être testées via le traitement des données et/ou au niveau du capteur.
- Afin de former l'image thermique, plusieurs techniques de balayage du capteur devront être identifiées, analysées et éventuellement testées/implémentées sur le dispositif (au moins une pour former l'image !). En parallèle, il conviendra également de déterminer la projection la mieux adaptée à la motorisation choisie pour former cette image et éventuellement d'analyser ses déformations.
- Esthétique, dimensions et ergonomie :
    - **Pas de parties saillantes pouvant blesser l’utilisateur**.
	- Il serait souhaitable que le dispositif puisse être fixé sur un trépieds grâce à l'ajout d'un écrou compatible avec la visse standard d'un trépieds.
	- Un soin tout particulier sera apporté à l’ergonomie et à l'esthétique du système.
	- L'impression 3D sera utilisée à bon escient, c.à.d. pour des pièces difficilement ajustables et/ou fabricables avec d'autres techniques et/ou matériaux.
	- Le système devra être facilement transportable et compact. Hors trépieds, **aucune des trois dimensions du dispositif ne devra dépasser 25cm**.
	- Afin d’éviter de l’abîmer pendant son déplacement, le système devra être bien protégé. Dans ce dernier cas, vous devrez veiller à ce que le système fonctionne dans les meilleures conditions (température, humidité, etc.).
	- Poids inférieur à **500g**.
- Les différents tests de votre dispositif se feront essentiellement sur des personnes immobiles placées en face de la caméra à une distance pouvant aller jusqu'à 2-3m. 
- Le prototype devra être bien décrit :
    - En terme de consommation. 
	- En terme d’autonomie pour un système sur batteries, le cas échéant : durée de vie, estimation du nombre d’images thermiques au maximum pouvant être acquises.
	- Estimation de la précision et de la reproductibilité des mesures.
	- Plans de réalisation de qualité. Utilisation de la 3D avec des logiciels comme [Solidworks](https://www.solidworks.com/fr) ou [FreeCad](https://www.freecadweb.org).
	- Logiciels mis en oeuvre bien commentés.
- Une (et une seule !) des options suivantes devra être implémentée suivant les affinités du groupe. Le choix de l'option devra être communiqué à votre tuteur le plus rapidement possible, idéalement avant la mi-novembre, car certains choix de composants peuvent être liés à l'option choisie.
    - **Option 1** : Pour les plus bricoleurs, réaliser le dispositif le plus autonome et le plus esthétique possible, ce qui implique un boitier, la possibilité de le fixer sur un trépieds, un fonctionnement sur batterie et le contrôle sans fils, par exemple avec un smartphone en wifi ou par le biais d'une interface graphique qui affiche les images (électronique plus conséquente dans ce dernier cas).
	- **Option 2** : Pour les physiciens, afin de minimiser les coûts, il est possible de choisir un capteur avec une résolution angulaire plus grande et de la réduire avec une optique adaptée; une recalibration de ce capteur devra être effectuée pour l'adapter à la nouvelle optique.
	- **Option 3** : Pour les informaticiens et mathématiciens, une caméra couleur (par exemple, une _webcam_) pourra être ajoutée au dispostif afin d'afficher une image en couleur superposée à l'image thermique sur un ordinateur. Cette option implique une bonne compréhension des phénomènes et abérations optiques qui se produisent dans une caméra, modèlisée entre autre par les paramètres intrinsèques de la caméra qu'il faudra donc estimer/déterminer par calibration. 
	- **Option 4** : Dans le même ordre d'idée que l'option précédente, une caméra 3D [OAK-D](https://shop.luxonis.com/products/oak-d) "intelligente" vous sera prêtée afin d'être utilisée pour cibler un objet et contrôler la position du dispositif pour prendre la température de cet objet, de préférence, de manière automatique; par exemple pour directement identifier une personne dans le champ de vue de la caméra par un alogrithme de reconnaissance de visage et prendre directement la température sur son front.
	- **Option 5** : Il vous égalemenent possible d'implémenter une option de votre choix qui devra être soumise à l'équipe organisatrice du projet pour acceptation. Les bonnes idées et l'esprit d'initiative sont toujours les bienvenues dans un projet.
- **Créer une vidéo finale récapitulative de votre projet** (à mettre en ligne, de préférence sur youtube avant la défense finale du projet) présentant votre dispositif, les logiciels développés avec le traitement effectué, le déroulement d'une acquisition, les résultats obtenus avec celui-ci c.à.d. plusieurs images thermiques que vous avez pu obtenir lors de vos tests ainsi que les problèmes identifiés et les solutions à ceux-ci éventuellement implémentées. Vous pouvez un peu vous inspirer des vidéos faites par les groupes de projets biomédicaux des années antérieures et accessibles sur le site du LISA en cliquant sur [le lien suivant](https://lisa.polytech.ulb.be/en/lisa-teaching/ba2-biomedical-projects).


Afin de pouvoir réaliser votre projet dans de bonnes conditions, vous trouverez à l'[adresse suivante](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/documentation/) une documentation que nous vous invitons à lire car elle apporte des compléments d'informations précieux pour le déroulement du projet. Notez que cette documentation pourrait faire encore l'objet de petites modifications au cours du premier semestre. Nous vous conseillons donc de la revoir de temps en temps.


## Répositoire gitlab

Dans le cadre du projet de cette année, un répositoire sur [_gitlab_](https://gitlab.com) devra être utilisé pour divers aspects de la réalisation de votre projet :

1. Documentation pour vos recherches bibliographiques.
2. Le développement de vos codes informatiques.
3. La publicité de vos tests/validations avec l’aide de pages webs générées à partir de fichiers _MarkDown_ (utilisation de _MkDocs_ avec les [_gitlab pages_](https://docs.gitlab.com/ee/user/project/pages/)). Des liens vers des vidéos externes (youtube ou autre) devraient être  idéalement inclus. 
4. **La gestion en continue de votre projet par le biais de “status reports”** rédigés avec des fichiers _MarkDown_ et l’inclusion de votre “Risks and Issue Logs” (cf. Gestion de Projet par Patrick Simon).
5. Remonter les problèmes à l’équipe organisatrice du projet et/ou à votre tuteur.

Diverses informations complémentaires vous sont fournies sur [une page de la documentation en ligne du projet](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/documentation/gitlab/). Un [template de projet “git”](https://gitlab.com/ulb-polytech/ba2-projects/biomed/2023-2024/ircam) est également partagé sur gitlab que vous pourrez cloner ou faire un _fork_ dans votre groupe _BIOMEDx_ (_x_ = 1 à 5, donc votre numéro de groupe). La version définitive de ce template sera prête début octobre mais n'hésitez pas à survoler déjà ce projet gitlab qui vous donne de nombreuses indications.

Vous serez invité d'ici fin septembre dans votre groupe privé _BIOMEDx_ avec votre adresse e-mail ULB. Vous devrez alors créer un compte sur [_gitlab.com_](https://gitlab.com) si vous n'en avez pas encore un.


## Budget et matériel

Le budget du projet est de maximum 100€. Dans la mesure du possible, la récupération sera donc privilégiée (p/ex pour les matériaux de construction : bois, etc, mais également l'écrou du trépieds qui pourra être récupérée sur un vieil appareil photo cassé par exemple, ...). Des composants électroniques (moteurs, etc) pourront être prêtés par le LISA pour tests s'ils sont disponibles (contact : [Rudy Ercek](mailto:rudy.ercek@ulb.be)) et certains intégrés au prototype (si disponibles en quantité suffisante !). Dans les autres cas, le matériel devra être acheté par les étudiants.

En pratique, il conviendra donc de distinguer deux types de budget : celui de tout le matériel acheté (incluant les composants pour tests, non utilisés ou cassés) et le coût réel du prototype intégrant une estimation du prix de tout le matériel utilisé pour sa fabrication (y compris récupération et fourni). Seul ce dernier pourra éventuellement un peu dépasser 100€ si cela se justifie.


## Ressources

- [Editeur de circuits en ligne](https://www.circuit-diagram.org/)
- Fournisseurs de composants électroniques :    
    - [Aliexpress](https://www.aliexpress.com) (bon marché mais en général lent, toutefois, livraison possible en 10 jours!)
	- [Amazon](https://www.amazon.de) (plus cher qu'Aliexpress mais plus rapide **si** expédié par amazon)
    - [Cotubex](https://www.cotubex.be) (magasin d'électronique mais fournitures très limitées)    	
- Impressions 3D : 
	- Au [FABLAB de l'ULB](http://fablab-ulb.be/equipements/) (contact : [Laurent Catoire](mailto:laurent.catoire@ulb.be)). Une formation sur l'impression 3D vous y sera donnée. Elle sera obligatoire pour un ou deux membres de chaque équipe de projet. Attention que le nombre de place sera limité.
	- Au [BEAMS](https://beams.polytech.ulb.be) (contact :[Maxime Verstraeten](mailto:maxime.verstraeten@ulb.be))
    - Au [LISA](https://lisa.polytech.ulb.be) - Imprimante type Prusa I3 pour des objets de dimensions inférieures à 15cm (contacts : [Thomas Vandamme](mailto:thomas.vandamme@ulb.be), [Rudy Ercek](mailto:rudy.ercek@ulb.be))
	- Eventuellement chez [PANORAMA](https://panorama.ulb.ac.be/notre-materiel/impression-3d/) (contact : [Henry-Louis Guillaume](mailto:henry-louis.guillaume@ulb.be))	
    - Base de données de pièces à imprimer : [thingiverse](https://www.thingiverse.com)
    - Création de pièces en ligne : [TinkerCad](https://www.tinkercad.com)
    - Logiciel gratuit de conception 3D : [FreeCad](https://www.freecadweb.org)
    - Logiciel (license universitaire) de conception 3D : [Solidworks](https://www.solidworks.com/fr)
- Découpes laser au [FABLAB de l'ULB](http://fablab-ulb.be/equipements/) (contact : [Laurent Catoire](mailto:laurent.catoire@ulb.be)). Vous devrez suivre une formation afin de pouvoir l'utiliser mais les places seront limitées, par défaut, à maximum 1 (ou 2) étudiants par groupe (sous réserve).


## Personnes ressources

- Recherche bibliographique : [Axel Dero](mailto:axel.dero@ulb.be)
- Support technique du projet : [Rudy Ercek](mailto:rudy.ercek@ulb.be)
- Programmation python : [Olivier Debeir](mailto:olivier.debeir@ulb.be), [Eline Soetens](mailto:eline.soetens@ulb.be), [Rudy Ercek](mailto:rudy.ercek@ulb.be) 
- Gestion de versions avec _git_ : [Eline Soetens](mailto:eline.soetens@ulb.be)
- Gestion des groupes : [Arthur Elskens](mailto:arthur.elskens@ulb.be), [Rudy Ercek](mailto:rudy.ercek@ulb.be), [Armand Losfield](mailto:armand.losfeld@ulb.be), [Eline Soetens](mailto:eline.soetens@ulb.be), [Maxime Verstraeten](mailto:maxime.verstraeten@ulb.be)
- Gestion de projet : [Patrick Simon](mailto:patrick.simon@ulb.be)
- Electronique : [Michel Osée](mailto:michel.osee@ulb.be), [Geoffrey Vanbienne](mailto:geoffrey.vanbienne@ulb.be)


## Calendrier

Voir guide du projet dans l'[Université Virtuelle](http://uv.ulb.ac.be)


**Date de la dernière révision de l'énoncé :**  {{ git_revision_date }}
