## ENONCE

[Enoncé du projet BA2 Biomédical 2023-2024](https://ulb-polytech.gitlab.io/ba2-projects/biomed/2023-2024/enonce/) de l'Ecole polytechnique de Bruxelles (ULB).

Basé sur le template [MkDocs](https://gitlab.com/pages/mkdocs) utilisant les [_Gitlab Pages_](https://pages.gitlab.io).


